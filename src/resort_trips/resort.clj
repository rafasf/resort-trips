(ns resort-trips.resort
  (:require [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [spec-tools.spec :as spec]))

(defn three-letter-gen []
  (gen/fmap
   #(apply str %)
   (gen/not-empty (gen/vector (gen/char-alpha) 3))))

(defn non-empty-str-gen []
  (gen/not-empty (gen/string-alphanumeric)))

(def non-empty-string-alphanumeric
  (s/with-gen
    spec/string?
    non-empty-str-gen))

(s/def ::name non-empty-string-alphanumeric)

(s/def ::address non-empty-string-alphanumeric)
(s/def ::address-country
  ;; Three-letter Country Code (ISO 3166-1 alpha-3))
  (s/with-gen
    (s/and spec/string? #(= (count %) 3))
    three-letter-gen))

(s/def ::latitude
  (s/double-in :min -90.0 :max 90.0 :NaN false :infinite? false))
(s/def ::longitude
  (s/double-in :min -180.0 :max 180.0 :NaN false :infinite? false))
(s/def ::geo
  ;; Subset of https://schema.org/GeoCoordinates
  (s/keys :req-un [::address ::address-country ::latitude ::longitude]))

(s/def ::iata-code
  (s/with-gen
    (s/and spec/string? #(= (count %) 3))
    three-letter-gen))

(s/def ::iata-codes (s/coll-of ::iata-code))
(s/def ::nearby ::iata-codes)
(s/def ::airports
  ;; Based on http://schema.org/Airport
  ;; Addition of nearby airports
  (s/keys :req-un [::iata-codes]
          :opt-un [::nearby]))

(s/def ::resort
  ;; Based on http://schema.org/SkiResort
  ;; Addition of Airports
  (s/keys :req-un [::name ::geo ::airports]))
