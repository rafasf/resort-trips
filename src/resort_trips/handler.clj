(ns resort-trips.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [resort-trips.resort :as resorts]))

(def app
  (api
   {:swagger
    {:ui "/"
     :spec "/swagger.json"
     :data {:info {:title "Resort Trips"
                   :description "The Resort Trips"}
            :tags [{:name "resorts", :description "Manage the Resorts"}]}}}

   (context "/resorts" []
     :tags ["resorts"]

     :coercion :spec

     (POST "/" []
       :return ::resorts/resort
       :body [resort ::resorts/resort]
       :summary "Creates a new Resort"
       (created "http://localhost/resorts" resort)))))
