 (defproject resort-trips "0.1.0-SNAPSHOT"
   :description "Making Resort Trips Easier"
   :dependencies [[org.clojure/clojure "1.9.0"]
                  [metosin/compojure-api "2.0.0-alpha20"]
                  [metosin/spec-tools "0.7.1"]
                  [manifold "0.1.6"]]
   :ring {:handler resort-trips.handler/app}
   :uberjar-name "server.jar"
   :profiles {:dev {:dependencies [[javax.servlet/javax.servlet-api "3.1.0"]
                                   [cheshire "5.5.0"]
                                   [ring/ring-mock "0.3.0"]
                                   [cljfmt "0.5.1"]
                                   [org.clojure/test.check "0.10.0-alpha3"]
                                   [midje "1.9.2"]]
                    :plugins [[lein-ring "0.12.4"]
                              [lein-midje "3.2.1"]
                              [lein-cloverage "1.0.13"]]}})
