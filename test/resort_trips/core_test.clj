(ns resort-trips.core-test
  (:require [cheshire.core :as cheshire]
            [midje.sweet :refer :all]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]
            [resort-trips.handler :refer :all]
            [resort-trips.resort :as resorts]
            [ring.mock.request :as mock]))

(defn a-resort []
  (first (gen/sample (s/gen ::resorts/resort) 1)))

(defn parse-body [body]
  (cheshire/parse-string (slurp body) true))

(facts "about /reports"
       (fact "POST / returns the request"
             (let [resort (a-resort)
                   response (app (-> (mock/request :post "/resorts")
                                     (mock/content-type "application/json")
                                     (mock/body (cheshire/generate-string resort))))
                   body (parse-body (:body response))]
               (:status response) => 201
               body => resort)))
